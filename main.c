#include <stdio.h>
#include <mpi.h>
#include <time.h>
#include <stdlib.h>

int isMultipleOfThree(int number) {
    return number % 3 == 0;
}

int randomInt(int min, int max) {
    int range = (max - min);
    int div = RAND_MAX / range;
    return (min + (rand() / div)) % range;
}

void printArray(int arr[], int size) {
    printf("[ ");
    for (int i = 0; i < size; i++) {
        printf("%d ", arr[i]);
    }
    printf("]\n");
}

void fillArray(int* arr, int size) {
    for (int i = 0; i < size; i++) {
        int n = randomInt(0, 10);
        arr[i] = n;
    }
}

int main(int argc, char* argv[]) {
    int rootRank = 0;
    int num, rank;
    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &num);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    if (!isMultipleOfThree(num)) {
        int errorCode = -1;
        MPI_Abort(MPI_COMM_WORLD, errorCode);
        return errorCode;
    }

    int columns = 3;
    int rows = num / 3;
    int numbers[rows];

    srand(clock() * rank);

    if (rank < columns) {
        fillArray(numbers, rows);
    }

    if (rank < columns) {
        printf("Process %d: ", rank);
        printArray(numbers, rows);
    }

    double start = MPI_Wtime();

    int dims[2] = {rows, columns};
    int period[2] = {1, 0};
    int reorder = 0;

    MPI_Comm cart;
    MPI_Cart_create(MPI_COMM_WORLD, 2, dims, period, reorder, &cart);

    MPI_Comm column;
    int remainDims[2] = {1, 0};
    MPI_Cart_sub(cart, remainDims, &column);

    int number;
    MPI_Scatter(&numbers, 1, MPI_INT, &number, 1, MPI_INT, rootRank, column);
    printf("proc %d received %d\n", rank, number);

    if (rank == rootRank) {
        printf("Total time: %f ms\n", (MPI_Wtime() - start) * 1000);
    }

    MPI_Finalize();
    return 0;
}
